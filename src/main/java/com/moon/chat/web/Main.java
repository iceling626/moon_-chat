package com.moon.chat.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class Main {

	
	@RequestMapping({"/","/home","/*.html"})
	public String home() {
	    return "home.html";
	}
	
	
	
}
