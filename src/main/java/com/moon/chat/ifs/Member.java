package com.moon.chat.ifs;

public interface Member {
	public String getUserId();
	public String getUserName();
}
