package com.moon.chat.entity;

import com.moon.chat.common.ChatSocket;
import com.moon.chat.ifs.Member;

public class RoomMember implements Member {
	
	String roomId;
	User user;
	ChatSocket socket;
	
	public RoomMember() {};
	public RoomMember(User user) {
		this.user = user;
	}
	public RoomMember(User user, ChatSocket socket, String roomId) {
		this.user = user;
		this.socket = socket;
		this.roomId = roomId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public ChatSocket getSocket() {
		return socket;
	}
	public void setSocket(ChatSocket socket) {
		this.socket = socket;
	}
	@Override
	public String getUserId() {
		return this.user.getId();
	}
	@Override
	public String getUserName() {
		return this.user.getName();
	}
	@Override
	public String toString() {
		return "RoomMember [roomId=" + roomId + ", user=" + user + "]";
	}
	
}
