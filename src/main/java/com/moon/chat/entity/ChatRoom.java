package com.moon.chat.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

import com.moon.chat.common.ChatSocket;

public class ChatRoom {
	String id;
	List<RoomMember> members;
	
	public ChatRoom() {};
	public ChatRoom(String id) {
		this.id = id;
		this.members = new ArrayList<RoomMember>();
	}
	public synchronized void addMember(RoomMember member) {
		if(members == null) {
			members = new ArrayList<RoomMember>();
		}
		members.add(member);
	}
	public synchronized int removeMember(String userId) {
		for(RoomMember rm : members) {
			if(rm.getUser().getId().equals(userId)) {
				members.remove(rm);
				return 1;
			}
		}
		return -1;
	}
	
	public CopyOnWriteArraySet<ChatSocket> notifyMembers() {
		CopyOnWriteArraySet<ChatSocket> set = new CopyOnWriteArraySet<>();
		for(RoomMember m : members) {
			set.add(m.getSocket());
		}
		return set;
	}

	public List<RoomMember> getMembers() {
		return members;
	}

	public void setMembers(List<RoomMember> members) {
		this.members = members;
	}
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(RoomMember m : members) {
			sb.append(m.getUser().getName()+" | ");
		}
		return "人数"+members.size()+" | " + sb ;
	}
	
	
	
}
