package com.moon.chat.common;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.moon.chat.entity.ChatRoom;
import com.moon.chat.entity.RoomMember;
import com.moon.chat.entity.User;

@ServerEndpoint(value = "/websocket/{name}/{roomId}")
@Component
public class ChatSocket {
    private static CopyOnWriteArraySet<ChatSocket> webSocketSet = new CopyOnWriteArraySet<ChatSocket>();
    private static Map<Session, RoomMember> connectmap = new HashMap<>();
    private static Map<String, ChatRoom> chatroom = new HashMap<>();
    private Session session;
    
    @OnOpen
    public void onOpen(Session session, @PathParam("name") String name, 
    									@PathParam("roomId") String roomId) {
        this.session = session;
        if(connectmap.get(session) != null) {
        	connectmap.get(session).getSocket().onClose();
        }
        
        User user = new User(session.getId(), name);
        RoomMember member = new RoomMember(user, this, roomId);
        connectmap.put(session, member);
        webSocketSet.add(this);
        
    	jionRoom(roomId, session.getId());
    	
        System.out.println(name+" 已上线！进入了房间"+roomId);
        String roomname = roomId.equals("0")?"大厅":roomId+"号聊天室";
        broadcast(name+" 已上线! 进入了 "+roomname);
    }
    
    @OnClose
    public void onClose() {
        RoomMember m = connectmap.get(session);
        String name = m.getUserName();
        webSocketSet.remove(this);
        leaveRoom(m.getRoomId(), m.getUserId());
        
        System.out.println(name+" 已下线，当前在线" + webSocketSet.size());
        broadcast(name+" 已下线");
        
        connectmap.remove(session);
    }
    
    @OnMessage
    public void onMessage(String message, Session session) {
        RoomMember m = connectmap.get(session);
        String name = m.getUserName();
        
        if(m.getRoomId()==null||m.getRoomId().equals(""))
        	broadcast(name+" 说："+message);
        else
        	broadcast(name+" 说："+message, m.getRoomId());
    }
    
    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }
    
    public String pack(String message) {
        RoomMember m = connectmap.get(session);
    	Map<String, String> resultMap = new HashMap<String, String>();
    	if(m != null) {
            String roomname = m.getRoomId().equals("0")?"大厅":m.getRoomId()+"号聊天室";
            
    		resultMap.put("roomId", roomname);
        	resultMap.put("chatroom", chatroom.get(m.getRoomId()).toString());
    	}
    	resultMap.put("count", webSocketSet.size()+"");
    	resultMap.put("message", message);
    	return JSON.toJSONString(resultMap);
    }
    
    public void broadcast(String message){
    	String result = pack("系统公告："+message);
    	
		for (ChatSocket item : webSocketSet) {
        	item.session.getAsyncRemote().sendText(result);
        }
    }
    
    public  void broadcast(String message, String roomId){
    	String result = pack(message);
    	
    	ChatRoom room = chatroom.get(roomId);
    	CopyOnWriteArraySet<ChatSocket> set = room.notifyMembers();
        for (ChatSocket item : set) {
        	item.session.getAsyncRemote().sendText(result);
        }
    }
    // 加入聊天室
    public void jionRoom(String roomId, String userId) {
    	ChatRoom r = chatroom.get(roomId);
    	if(r == null) {
    		chatroom.put(roomId, new ChatRoom(roomId));
    	}
    	chatroom.get(roomId).addMember(connectmap.get(session));
    }
    // 退出聊天室
    public void leaveRoom(String roomId, String userId) {
    	chatroom.get(roomId).removeMember(userId);
    }
    
}
